# -*- coding: utf-8 -*-
"""
Created on Wed Jan 03 18:36:37 2018

@author: peteh
"""

from trees import BinarySearchTree as BST  
import csv as csv

class SweepLine(object):
    def __init__(self):
        self.events = EventQueue()
        self.avl_tree = BST()

class EventQueue(object):
    def __init__(self):
        self.events = []
        
    def del_first_event(self):
        if len(self.events) > 1:
            del self.events[0]
        else:
            self.events = []
            
    def sort_events(self):
        sorted(self.events, key=lambda event:event.y)

    def load_data(self, path):
        self.data_frame = []
        with open(path, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.data_frame.append([float(item) for item in row])
        
        

class Event(object):
    def __init__(self, x, y, isleft, edgeid):
        self.x = x
        self.y = y
        self.isleft = isleft
        self.edgeid = edgeid
        
    def xyorder(self, event):
        if self.x > event.x: return 1
        if self.x < event.x: return -1
        if self.y > event.y: return 1
        if self.y < event.y: return -1
        return 0
        
