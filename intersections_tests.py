# -*- coding: utf-8 -*-
"""
Created on Wed Jan 03 12:50:49 2018

@author: peteh
"""

from timeit import default_timer as timer
import intersections as inter
from random import random

def function_tests():
    x0, y0 = 0.0,0.0
    x1, y1 = 100.0,100.0
    x2, y2 = 1.0,0.0
    x3, y3 = 0.0,1.0
    inter.segment2d_segment2d(x0, y0, x1, y1, x2, y2, x3, y3)
    N = 10000000
    point_array = [[random()*50 for i in range(8)] for j in range(N)]

    start = timer()
    counter = 0 
    for segments in point_array:
        x0, y0, x1, y1, x2, y2, x3, y3 = segments
        out = inter.segment2d_segment2d(x0, y0, x1, y1, x2, y2, x3, y3)
        if out[0] == 1:
            counter += 1
            
            
    runtime = timer() - start
    print('Intersections completed in {0}s'.format(runtime))
    print('{0} intersections found'.format(counter))
    

    
if __name__ == '__main__':
    function_tests()