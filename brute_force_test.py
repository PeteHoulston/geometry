# -*- coding: utf-8 -*-
"""
Created on Wed Jan 03 20:17:20 2018

@author: peteh
"""

import csv as csv
from intersections import segment2d_segment2d as inter
from timeit import default_timer as timer
from math import sqrt
import numpy as np

def check(row, sub_row):
    x, y, z, idi = 0,0,0,0
    x0 = row[0]
    y0 = row[1]
    x1 = row[3]
    y1 = row[4]
    x2 = sub_row[0]
    y2 = sub_row[1]
    x3 = sub_row[3]
    y3 = sub_row[4]
    output = inter(x0, y0, x1, y1, x2, y2, x3, y3)
    if output[0] == 1:
        L = sqrt((x3-x2)**2+(y3-y2)**2)
        Li = sqrt((output[1]-x2)**2+(output[2]-y2)**2)
        dz = sub_row[2] - sub_row[6]
        z = sub_row[2] - Li*dz/L
        return [output[1],output[2],z,sub_row[6]]
    else:
        return [x,y,z,idi]
    
def worker(row, dataframe):
    intersections = [0]
    x0 = row[0]
    y0 = row[1]
    x1 = row[3]
    y1 = row[4]
    for j, sub_row in enumerate(dataframe):
        if sub_row[6] != 0:
            intersections.append(check(row,sub_row))
            """
            x2 = sub_row[0]
            y2 = sub_row[1]
            x3 = sub_row[3]
            y3 = sub_row[4]
            output = inter(x0, y0, x1, y1, x2, y2, x3, y3)
            if output[0] == 1:
                L = sqrt((x3-x2)**2+(y3-y2)**2)
                Li = sqrt((output[1]-x2)**2+(output[2]-y2)**2)
                dz = sub_row[2] - sub_row[6]
                z = sub_row[2] - Li*dz/L
                intersections.append([output[1],output[2],z,sub_row[6]])
            """
            """
                if row[7] in intersections:
                    intersections[row[7]].append([output[1],output[2],z,sub_row[6]])
                else:
                    intersections[row[7]] = [[output[1],output[2],z,sub_row[6]]]
            """
            
                
    return intersections[1:] if len(intersections) > 1 else []


def main():
    path = r'data\output.csv'
    dataframe = []
    with open(path, 'r') as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            dataframe.append([float(item) for item in row])
      
    dataframe = np.array(dataframe)
    print(dataframe[:3])
    print(len(dataframe))
    
    counter = 0
    start = timer()
    intersections = {}
    """
    for i, row in enumerate(dataframe):
        new_intersections = worker(row, dataframe)
        if len(new_intersections) > 0:
            intersections.append(new_intersections)
        
    """
        
    
    for i, row in enumerate(dataframe):
        if row[7] != 0:
            x0 = row[0]
            y0 = row[1]
            x1 = row[3]
            y1 = row[4]
            for j, sub_row in enumerate(dataframe):
                if sub_row[6] != 0:
                    x2 = sub_row[0]
                    y2 = sub_row[1]
                    x3 = sub_row[3]
                    y3 = sub_row[4]
                    output = inter(x0, y0, x1, y1, x2, y2, x3, y3)
                    if output[0] == 1:
                        L = sqrt((x3-x2)**2+(y3-y2)**2)
                        Li = sqrt((output[1]-x2)**2+(output[2]-y2)**2)
                        dz = sub_row[2] - sub_row[6]
                        z = sub_row[2] - Li*dz/L
                        counter += 1
                        if row[7] in intersections:
                            intersections[row[7]].append([output[1],output[2],z,sub_row[6]])
                        else:
                            intersections[row[7]] = [[output[1],output[2],z,sub_row[6]]]
    
    runtime = timer() - start         
    print('count = ' + str(counter))
    print('time = {0}s'.format(runtime))
    print(intersections[1])
    
if __name__ == '__main__':
    main()