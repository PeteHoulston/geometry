# -*- coding: utf-8 -*-
"""
Created on Wed Jan 03 11:37:27 2018

@author: peteh
"""


def line2d_line2d(a1, b1, c1, a2, b2, c2):
    d =  a1*b2-a2*b1
    if d == 0:
        return 0
    
    d = 1/d
    return (c1*b2-c2*b1)*d,(a1*c2-a2*c1)*d  

def line3d_line3d(ox0, oy0, oz0, vx0, vy0, vz0,
                  ox1, oy1, oz1, vx1, vy1, vz1):
    pass

def segment2d_segment2d(x0, y0, x1, y1, x2, y2, x3, y3):
    s10_x = x1 - x0
    s10_y = y1 - y0
    s32_x = x3 - x2
    s32_y = y3 - y2
    denom = s10_x * s32_y - s32_x * s10_y

    if denom == 0: 
        return [0,0,0] # collinear

    denom_is_positive = denom > 0
    s02_x = x0 - x2
    s02_y = y0 - y2
    s_numer = s10_x * s02_y - s10_y * s02_x

    if (s_numer < 0) == denom_is_positive: 
        return [0,0,0] # no collision

    t_numer = s32_x * s02_y - s32_y * s02_x

    if (t_numer < 0) == denom_is_positive: 
        return [0,0,0] # no collision

    if (s_numer > denom) == denom_is_positive or (t_numer > denom) == denom_is_positive: 
        return [0,0,0] # no collision
    
    # collision detected
    t = t_numer / denom
    intersection_point = [1,x0 + (t * s10_x), y0 + (t * s10_y)]
    return intersection_point

def segment3d_segment3d(x00, y00, z00, x01, y01, z01,
                        x10, y10, z10, x11, y11, z11):
    pass

def circle2d_circle2d(x0, y0, r0, x1, y1, r1):
    pass

def quad2d_quad2d(x00, y00, x01, y01, x02, y02, x03, y03,
                  x10, y10, x11, y11, x12, y12, x13, y13):
    pass

def plane_plane(ox0, oy0, oz0, nx0, ny0, nz0,
                ox1, oy1, oz1, nx1, ny1, nz1):
    pass

def plane_segment3d(ox0, oy0, oz0, nx0, ny0, nz0,
                    x00, y00, z00, x01, y01, z01):
    pass